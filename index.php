<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lam bai trac nghiem nao !</title>
    <link rel="stylesheet" href="style.css">
</head>

<?php
        session_start();
        $question = array(
            1 => "Question 1?", 
            2 => "Question 2?",
            3 => "Question 3?", 
            4 => "Question 4?",
            5 => "Question 5?", 
            6 => "Question 6?",
            7 => "Question 7?",
            8 => "Question 8?",
            9 => "Question 9?", 
            10 => "Question 10?"
            );

        $answer = array(
            1 => array("Sai1"=>0, "Sai2"=>0, "Đúng"=>1, "Sai3"=>0), 
            2 => array("Sai1"=>0, "Đúng"=>1, "Sai2"=>0, "Sai3"=>0),
            3 => array("Sai1"=>0, "Sai2"=>0, "Sai3"=>0, "Đúng"=>1), 
            4 => array("Sai1"=>0, "Sai2"=>0, "Đúng"=>1, "Sai3"=>0),
            5 => array("Đúng"=>1, "Sai1"=>0, "Sai2"=>0, "Sai3"=>0), 
            6 => array("Đúng"=>1, "Sai1"=>0, "Sai2"=>0, "Sai3"=>0),
            7 => array("Sai1"=>0, "Đúng"=>1, "Sai2"=>0, "Sai3"=>0), 
            8 => array("Sai1"=>0, "Sai2"=>0, "Đúng"=>1, "Sai3"=>0),
            9 => array("Sai1"=>0, "Đúng"=>1, "Sai2"=>0, "Sai3"=>0), 
            10 => array("Sai1"=>0, "Sai2"=>0, "Sai3"=>0, "Đúng"=>1)
        );

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $_SESSION = $_POST;
            header("Location: page2.php");
        }
    ?>

<body>
    <form method="post">
        <div class="container">
            <div class="container__header">
                <div class="header--bg"></div>
                <div class="header--title">
                    <h1>Trắc nghiệm</h1>
                </div>
            </div>
            <div class="container__content">
                <div class="content--title">
                    Làm nhanh không hết giờ.
                </div>

                <?php
                    foreach ($question as $key => $value) {
                        if ($key <= 5){
                            echo '<div class="content-item">';
                            echo '<div class="item__top">';
                            echo "Câu {$key}: {$value}";
                            echo '</div>';
                            foreach ($answer as $key_ans => $value_ans) {
                                if ($key == $key_ans){
                                    echo '<div class="item__center">';
                                    foreach ($value_ans as $vlue => $dan){
                                        echo '<div>';
                                        echo "<input type=\"radio\" name=\"question_{$key_ans}\" value=\"{$vlue}\"> {$vlue}<br>";
                                        echo '<label"></label><br>';
                                        echo '</div>';
                                    }
                                    echo '</div>';
                                }
                            }
                            echo '</div>';
                        }
                    };
                ?>
            </div>
    </form>

    <button class="button-next">Next</button>
    </div>
</body>

</html>